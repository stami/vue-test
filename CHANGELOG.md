# 1.0.1 (2022-02-14)

## Features

- __Logs update__: [Tree](https://gitlab.com/stami/vue-test/-/merge_requests/9)
- __Logs delete__ [Tree](https://gitlab.com/stami/vue-test/-/merge_requests/8)
- __Logs read__ [Tree](https://gitlab.com/stami/vue-test/-/merge_requests/7)
- __Add logs__ [Tree](https://gitlab.com/stami/vue-test/-/merge_requests/6)


# 1.0.0 (2022-02-06)

## Features

- __ENV__: Base environment configuration + User Login [Tree](https://gitlab.com/stami/vue-test/-/merge_requests/1)
- __Getting IP data__: Getting IP data component  [Tree](https://gitlab.com/stami/vue-test/-/merge_requests/2)
- __Logs List__: Add User DRF Authentication with JWT Token [Tree](https://gitlab.com/stami/vue-test/-/merge_requests/3)
