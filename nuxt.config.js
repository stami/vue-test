export default {
  // Disable server-side rendering: https://go.nuxtjs.dev/ssr-mode
  ssr: false,

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'app',
    htmlAttrs: {
      lang: 'en',
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
    script: [
      {
        src: 'https://kit.fontawesome.com/80d26400c5.js',
        async: true,
        defer: true,
      },
    ],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ['~/assets/css/main.scss', 'v-tooltip/dist/v-tooltip.css'],

  /* Page Transitions */
  layoutTransition: {
    name: 'default',
    mode: 'out-in',
  },
  pageTransition: {
    name: 'default',
    mode: 'out-in',
  },

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: ['~/plugins/errorHandle.js', '~/plugins/tooltip.js'],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/tailwindcss
    '@nuxtjs/tailwindcss',
    '@nuxtjs/composition-api/module',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    '@nuxtjs/auth-next',
    '@nuxtjs/dayjs',
    'portal-vue/nuxt',
    '@nuxtjs/toast',
  ],

  router: {
    middleware: ['auth'],
  },

  auth: {
    redirect: {
      login: '/login/',
      logout: '/login/',
      home: '/',
    },
    cookie: false,
    strategies: {
      local: {
        scheme: 'refresh',
        token: {
          property: 'access',
          maxAge: 900,
          global: true,
          type: 'Bearer',
        },
        refreshToken: {
          property: 'refresh',
          data: 'refresh',
          maxAge: 60 * 60 * 24,
        },
        user: {
          property: false,
        },
        endpoints: {
          login: { url: '/token/', method: 'post' },
          refresh: {
            url: '/token/refresh/',
            method: 'post',
          },
          user: {
            url: '/users/me/',
            method: 'get',
            propertyName: false,
          },
          logout: false,
        },
        // autoLogout: false
      },
    },
    plugins: ['~/plugins/checkPrivileges.js'],
  },

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    baseURL: process.env.API_URL,
  },

  publicRuntimeConfig: {
    dateFormat: 'YYYY-MM-DD',
    timeFormat: 'HH:mm',
    dateTimeFormat: 'YYYY-MM-DD HH:mm',
  },

  dayjs: {
    locales: ['pl'],
    defaultLocale: 'pl',
    defaultTimeZone: 'Europe/Warsaw',
    plugins: [
      'utc', // import 'dayjs/plugin/utc'
      'timezone', // import 'dayjs/plugin/timezone'
    ], // Your Day.js plugin
  },

  toast: {
    position: 'top-right',
    duration: 4000,
    keepOnHover: true,
    register: [
      // Register custom toasts
      {
        name: 'my-error',
        message: 'Ups...Something went wrong',
        options: {
          type: 'error',
        },
      },
    ],
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {},
}
