export const state = () => ({
  errorCode: undefined,
  errorMessage: undefined,
})

export const getters = {
  getErrorCode(state) {
    return state.errorCode
  },
  getErrorMessage(state) {
    return state.errorMessage
  },
}

export const mutations = {
  setCode(state, errorCode) {
    state.errorCode = errorCode
  },
  setMessage(state, errorMessage) {
    state.errorMessage = errorMessage
  },
}
