// Checks privileges due to user's group names
// v-if="this.$checkPrivileges(['superuser', 'parent'])"  # true if user has group 'superuser' or 'parent'

export default function ({ app }, inject) {
  inject('checkPrivileges', (allowedGroups) => {
    return app.$auth.user.groups.some((r) => allowedGroups.includes(r))
  })
}
