const removeAllErrors = () => {
  const errorMessagesToRemove = document.querySelectorAll(
    '.error-message, .error-icon'
  )
  errorMessagesToRemove.forEach((e) => e.remove())
  const errorFields = document.querySelectorAll('.error-field')
  errorFields.forEach((e) => e.classList.remove('error-field'))
}

const addError = (el, k, v) => {
  const errorParagraph = document.createElement('p')
  errorParagraph.setAttribute(
    'class',
    'my-2 text-sm text-red-600 error-message'
  )

  const errorIcon = document.createElement('div')
  errorIcon.setAttribute(
    'class',
    'error-icon absolute top-3 right-0 pr-3 flex items-center pointer-events-none'
  )
  errorIcon.innerHTML = '<i class="fal fa-exclamation-circle text-red-500"></i>'

  let errorMessage = ''
  if (Array.isArray(v) && v.length) {
    v.forEach((error, i) => {
      errorMessage += `<li>${error}</li>`
    })
  } else if (v.length) {
    errorMessage = `<li>${v}</li>`
  }

  errorParagraph.innerHTML = `<ul class="list-none p-0 m-0">${errorMessage}</ul>`
  el.after(errorIcon)
  el.after(errorParagraph)
}

const extractErrors = (errorData) => {
  if (typeof errorData === 'object') {
    for (const [k, v] of Object.entries(errorData)) {
      const el = document.querySelector(`[name=${k}]`)
      if (el && v.length) {
        el.classList.add('error-field')
        addError(el, k, v)
      }
    }
  }
}

export default function ({ store, $axios, $toast }) {
  $axios.onError((error) => {
    store.commit('errorhandle/setCode', undefined)
    store.commit('errorhandle/setMessage', undefined)

    if (error.response.data.constructor === Object) {
      const errorData = error.response.data || null
      const errorCode = error.response.status || null

      if (errorData) {
        removeAllErrors()
        if (errorData.detail) {
          // General error info
          store.commit('errorhandle/setCode', errorCode)
          store.commit('errorhandle/setMessage', errorData.detail)
        } else {
          extractErrors(errorData)
        }
      }
    } else if (error.request) {
      const errorData =
        `${error.request.responseURL} - ${error.request.statusText}` || null
      const errorCode = error.request.status || null
      store.commit('errorhandle/setCode', errorCode)
      store.commit('errorhandle/setMessage', errorData)
    } else {
      const errorData = error.message || null
      const errorCode = 'Unknown'
      store.commit('errorhandle/setCode', errorCode)
      store.commit('errorhandle/setMessage', errorData)
    }
  })
}
